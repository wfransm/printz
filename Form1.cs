﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FireSharp.Config;
using FireSharp.Response;
using FireSharp.Interfaces;
using FireSharp;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using RawPrint;
using System.Diagnostics;
using System.Threading;

namespace PrintZ
{
    public partial class Form1 : Form
    {
        WebClient clientWeb = new WebClient();
        public Form1()
        {
            InitializeComponent();
            showPrint();
        }

        IFirebaseConfig ifC = new FirebaseConfig() 
        {
            AuthSecret = "9z3LJPa3UfcJn8vOZfawbXQZu2JxSjqId1Gek49D",
            BasePath = "https://printq-b8807-default-rtdb.firebaseio.com/"
        };
        IFirebaseClient client;
        private void Form1_Load(object sender, EventArgs e)
        {
            try 
            {
                client = new FirebaseClient(ifC);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        async void LiveData()
        {
            while (true)
            {
                await Task.Delay(1000);
                FirebaseResponse res = await client.GetTaskAsync(@"task/print");
                if (res.Body != "null")
                {
                    Dictionary<string, Prints> data = res.ResultAs<Dictionary<string, Prints>>();
                    UpdateData(data);
                }
                else
                {
                    txtCount.Text = null;
                }
                //try
                //{
                //    Dictionary<string, Prints> data = res.ResultAs<Dictionary<string, Prints>>();
                //    if (data != null)
                //    {
                //        UpdateData(data);
                //    }
                //}
                //catch (Exception th)
                //{
                //    Console.WriteLine(th);
                //}
            }
        }

        void UpdateData(Dictionary<string, Prints> record)
        {
            listTask.Items.Clear();
            txtCount.Text = record.Count().ToString();

            foreach (var get in record)
            {
                if (get.Value.Status == "on")
                {
                    ListViewItem lvi = new ListViewItem(get.Value.Idprint);
                    lvi.SubItems.Add(get.Value.Url);
                    lvi.SubItems.Add(get.Value.Status);

                    listTask.Items.Add(lvi);
                }
            }

            try 
            {
                
                txtIdPrint.Text = listTask.Items[0].SubItems[0].Text;
                txtTask.Text = listTask.Items[0].SubItems[1].Text;

                unduhFile();

                Prints pr = new Prints()
                {
                    Idprint = listTask.Items[0].SubItems[0].Text,
                    Status = "off",
                    Url = listTask.Items[0].SubItems[1].Text
                };

                var setter = client.Update("task/print/"+ listTask.Items[0].SubItems[0].Text, pr);
            }
            catch (Exception th)
            {
                Console.WriteLine(th.Message);
                txtIdPrint.Text = null;
                txtTask.Text = null;
            }
        }
        IPrinter printer = new Printer();

        private const int NumberOfRetries = 3;
        private const int DelayOnRetry = 1000;

        void unduhFile()
        {
            string url = txtTask.Text;
            //string url = "https://zx.wijayaplywoodsindonesia.com/api/print-e/download-nota/sj2894.pdf";
            //string printerName = "Microsoft Print to PDF";
            //string printerName = "EPSON L120 Series";
            string printerName = comboBox1.Text;
            Uri uri = new Uri(url);
            IPrinter printer = new Printer();
            try
            {
                string fileName = Path.GetFileName(uri.AbsolutePath);
                clientWeb.DownloadFileAsync(uri, fileName);

                string filePath = Path.Combine(Environment.CurrentDirectory, fileName);
                
                try 
                {
                    //printer.PrintRawFile(printerName, filePath, fileName);
                    int urutan = 0;
                    for (int i = 1; i <= NumberOfRetries; ++i)
                    {
                        try
                        {
                            //printer.PrintRawFile(printerName, filePath, fileName);
                            using (StreamReader reader = new StreamReader(filePath))
                            {
                                urutan = i;
                                //Console.WriteLine("open file +++++++++++++++++++++++++++++++++++++++ ke-"+urutan);
                                //reader.Close();
                            }
                            break;
                        }
                        catch (IOException e) when (i <= NumberOfRetries)
                        {
                            Thread.Sleep(DelayOnRetry);
                        }
                    }

                    if (urutan >= 2 )
                    {
                        if (ckPrint.Checked)
                        {
                            printer.PrintRawFile(printerName, filePath, fileName);
                        }
                        else
                        {
                            ProcessStartInfo printInfo = new ProcessStartInfo()
                            {
                                Verb = "print",
                                CreateNoWindow = true,
                                FileName = filePath,
                                WindowStyle = ProcessWindowStyle.Hidden,
                            };

                            Process printProses = new Process();
                            printProses.StartInfo = printInfo;
                            printProses.Start();

                            printProses.WaitForInputIdle();
                            Thread.Sleep(3000);

                            try
                            {
                                if (false == printProses.CloseMainWindow())
                                {
                                    printProses.Kill();
                                }
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("error print " + e);
                            }
                        }
                    }
                } catch (Exception thh) 
                {
                    Console.WriteLine("=======================");
                    Console.WriteLine("error print " + thh);
                    Console.WriteLine("=======================");
                }

            }
            catch (Exception th)
            {
                Console.WriteLine("error download "+th);
            }
        }
        
        void getFile()
        {
            string printerName = "Microsoft XPS Document Writer";
            string fileName = "BK.pdf";
            string filePath = Path.Combine(Environment.CurrentDirectory, fileName);

            ProcessStartInfo printInfo = new ProcessStartInfo() {
                Verb = "print",
                CreateNoWindow = true,
                FileName = filePath,
                WindowStyle = ProcessWindowStyle.Hidden,
                //Arguments = "\""+printerName+"\""
            };

            Process printProses = new Process();
            printProses.StartInfo = printInfo;
            printProses.Start();

            printProses.WaitForInputIdle();
            Thread.Sleep(3000);

            try
            {
                if (false == printProses.CloseMainWindow())
                {
                    printProses.Kill();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("error print "+e);
            }
            
        }

        void showPrint()
        {
            foreach (string printerName in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                comboBox1.Items.Add(printerName);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LiveData();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Console.WriteLine(ckPrint.Checked.ToString());
            
            //getFile();
        }
    }
}
