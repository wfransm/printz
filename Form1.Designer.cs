﻿
namespace PrintZ
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.listTask = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.txtCount = new System.Windows.Forms.TextBox();
            this.txtTask = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.txtIdPrint = new System.Windows.Forms.TextBox();
            this.ckPrint = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(311, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // listTask
            // 
            this.listTask.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.listTask.HideSelection = false;
            this.listTask.Location = new System.Drawing.Point(27, 53);
            this.listTask.Name = "listTask";
            this.listTask.Size = new System.Drawing.Size(381, 236);
            this.listTask.TabIndex = 1;
            this.listTask.UseCompatibleStateImageBehavior = false;
            this.listTask.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Id";
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Url";
            this.columnHeader2.Width = 252;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Status";
            // 
            // txtCount
            // 
            this.txtCount.Location = new System.Drawing.Point(27, 15);
            this.txtCount.Name = "txtCount";
            this.txtCount.Size = new System.Drawing.Size(98, 20);
            this.txtCount.TabIndex = 2;
            // 
            // txtTask
            // 
            this.txtTask.Location = new System.Drawing.Point(414, 85);
            this.txtTask.Name = "txtTask";
            this.txtTask.Size = new System.Drawing.Size(286, 20);
            this.txtTask.TabIndex = 3;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(414, 125);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(85, 25);
            this.button2.TabIndex = 4;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(551, 149);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(149, 21);
            this.comboBox1.TabIndex = 5;
            // 
            // txtIdPrint
            // 
            this.txtIdPrint.Location = new System.Drawing.Point(414, 57);
            this.txtIdPrint.Name = "txtIdPrint";
            this.txtIdPrint.Size = new System.Drawing.Size(70, 20);
            this.txtIdPrint.TabIndex = 6;
            // 
            // ckPrint
            // 
            this.ckPrint.AutoSize = true;
            this.ckPrint.Location = new System.Drawing.Point(551, 125);
            this.ckPrint.Name = "ckPrint";
            this.ckPrint.Size = new System.Drawing.Size(66, 17);
            this.ckPrint.TabIndex = 8;
            this.ckPrint.Text = "print raw";
            this.ckPrint.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(726, 308);
            this.Controls.Add(this.ckPrint);
            this.Controls.Add(this.txtIdPrint);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.txtTask);
            this.Controls.Add(this.txtCount);
            this.Controls.Add(this.listTask);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ListView listTask;
        private System.Windows.Forms.TextBox txtCount;
        private System.Windows.Forms.TextBox txtTask;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.TextBox txtIdPrint;
        private System.Windows.Forms.CheckBox ckPrint;
    }
}

